<?php
    // setup the URL and read the CSS from a file
    $url = 'https://cssminifier.com/raw';
    $css = '/* Default CSS Modify */
.elementor-widget:not(:last-child) {
    margin: 0px !important;
}

.elementor-column-wrap {
    padding: 0px !important;
}
/* Default CSS Modify END*/
/* CSS For Yahoo */
h1 {
	all: unset;
    margin: 17px 0px;
    display: block;
}
h2,
h3,
h4,
h5,
h6,
span,
p {
	all: unset;
    margin: 17px auto;
    display: block;
}
img {
	width: 100%;
}
/* CSS For Yahoo END*/
/* typography start*/
a {
    text-decoration: none;
    box-shadow: none;
    cursor: pointer;
}';

    // init the request, set various options, and send it
    $ch = curl_init();

    curl_setopt_array($ch, [
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => true,
        CURLOPT_HTTPHEADER => ["Content-Type: application/x-www-form-urlencoded"],
        CURLOPT_POSTFIELDS => http_build_query([ "input" => $css ])
    ]);

    $minified = curl_exec($ch);

    // finally, close the request
    curl_close($ch);

    // output the $minified css
    echo $minified;
?>